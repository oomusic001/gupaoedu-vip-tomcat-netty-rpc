package com.gupao.vip.nio.tomcat.servlet;

import com.gupao.vip.nio.tomcat.http.GPRequest;
import com.gupao.vip.nio.tomcat.http.GPResponse;
import com.gupao.vip.nio.tomcat.http.GPServlet;

public class FirstServlet extends GPServlet {
    public void doGet(GPRequest request, GPResponse response){
        this.doPost(request,response);
    }
    public void doPost(GPRequest request,GPResponse response){
        response.write("This is First Servlet.");
    }
}
