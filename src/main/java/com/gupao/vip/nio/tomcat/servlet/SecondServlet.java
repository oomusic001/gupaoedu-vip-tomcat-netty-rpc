package com.gupao.vip.nio.tomcat.servlet;

import com.gupao.vip.nio.tomcat.http.GPRequest;
import com.gupao.vip.nio.tomcat.http.GPResponse;
import com.gupao.vip.nio.tomcat.http.GPServlet;

public class SecondServlet extends GPServlet {
    @Override
    public void doGet(GPRequest request, GPResponse response) {
        doPost(request,response);
    }

    @Override
    public void doPost(GPRequest request, GPResponse response) {
        response.write("This is Second Servlet.");
    }
}
