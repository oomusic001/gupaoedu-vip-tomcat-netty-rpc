package com.gupao.vip.nio.tomcat.http;

import java.io.IOException;
import java.io.InputStream;

public class GPRequest {
    private String method;
    private String url;
    public GPRequest(InputStream inputStream){
        //从request()中取出数据
        String content = "";
        byte[] buf = new byte[1024];
        int len = 0;
        try {
            if ((len = inputStream.read(buf))>0){
                content = new String(buf,0,len);
            }
            String line = content.split("\\n")[0];
            String[] arr = line.split("\\s");
            this.method = arr[0];
            this.url = arr[1].split("\\?")[0];
            System.out.println(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String  getUrl() {
        return  this.url;

    }

    public String getMethod() {
        return this.method;
    }
}
