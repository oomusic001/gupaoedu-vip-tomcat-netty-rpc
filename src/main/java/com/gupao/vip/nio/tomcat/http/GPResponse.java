package com.gupao.vip.nio.tomcat.http;

import java.io.IOException;
import java.io.OutputStream;

public class GPResponse {
    OutputStream out;
    public void write(String s)  {
        StringBuilder sb = new StringBuilder();
        sb.append(s);
        try {
            sb.append("HTTP/1.1 200 OK \n").append("\r\n");
            out.write(sb.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public GPResponse(OutputStream outputStream){
        this.out = outputStream;
    }
}
