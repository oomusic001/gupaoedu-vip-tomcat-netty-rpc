package com.gupao.vip.nio.tomcat;

import com.gupao.vip.nio.tomcat.http.GPRequest;
import com.gupao.vip.nio.tomcat.http.GPResponse;
import com.gupao.vip.nio.tomcat.http.GPServlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class GPTomcat {

    private int port = 9980;//默认端口号
    private ServerSocket server;
    private Map<String ,GPServlet> servletMapping = new HashMap<>();
    private Properties webxml = new Properties();

    private void init(){
        try {
            //加载web.xml，同时初始化servletMapping对象
            String WEB_INF = this.getClass().getResource("/").getPath();
            FileInputStream fis = new FileInputStream(WEB_INF+"web.properties");
            webxml.load(fis);
            for (Object k : webxml.keySet()) {
                String key = k.toString();
                if (key.endsWith(".url")){
                    String servletName = key.replaceAll("\\.url$","");
                    String url = webxml.getProperty(key);
                    String className = webxml.getProperty(servletName + ".className");
                    GPServlet servlet = (GPServlet) Class.forName(className).newInstance();
                    servletMapping.put(url,servlet);

                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *  i启动tomcat
     */
    public void start(){
        //1.加载配置文件，初始化servletMapping
        init();
        try {
            server = new ServerSocket(this.port);
            System.out.println("tomcat 已经启动 ，监听的端口是："+this.port);
            //2.等待用户请求
            while (true){
                Socket client = server.accept();
                //http请求，发送的是数据是字符串。有规律的字符串，http协议；
                process(client);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void process(Socket client) throws IOException {
        InputStream inputStream = client.getInputStream();
        OutputStream outputStream = client.getOutputStream();
        GPRequest request = new GPRequest(inputStream);
        GPResponse response = new GPResponse(outputStream);
        //从协议中拿到url
        String url = request.getUrl();
        if (servletMapping.containsKey(url)){
            servletMapping.get(url).service(request,response);
        }else {
            response.write("404 not found");
        }
        outputStream.flush();
        outputStream.close();
        inputStream.close();
        client.close();

    }

    public static void main(String[] args) {
        new GPTomcat().start();
    }

    //j2ee标准
    //servlet
    //request
    //response

    //1.配置好启动端口， serverSocket，ip:localhost
    //2.配置web.xml,自己写的servlet继承httpservlet
    //servlet-name
    //servlet-class
    //url-pattern
    //3.读取配置 url-pattern , 和servlet建议一个映射关系
    // Map servletMapping

    //4.http请求，发送的数据就是字符串，有规律的字符串(http协议)
    //5.从协议中拿到url，把响应的servlet用反射进行实例化
    //6.调用实例化对象的service方法，执行具体的逻辑doGet,doPost方法。
    //7.request(对inputStream的封装)对象，response(对outputStream的封装)对象，
}
