package com.gupao.vip.nio.server;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * NIO的操作过于繁琐，于是又了netty，netty对nio进行了封装。
 */
public class NIOServerDemo {
    private  int port;
    //准备两个东西：轮询器selector(大堂经理)，缓冲区buffer(等候区)
    private Selector selector ;
    private ByteBuffer buffer = ByteBuffer.allocate(1024);
    //初始化，开门营业
    public NIOServerDemo(int port) {
        try {
            this.port = port;
            ServerSocketChannel serverSocket = ServerSocketChannel.open();
            //告诉别人地址：ip+port
            serverSocket.bind(new InetSocketAddress(this.port));
            //BIO升级为NIO后，为了兼容BIO，NIO模型默认采用阻塞式
            //手动设置为非阻塞
            serverSocket.configureBlocking(false);

            //大堂经理准备就绪
            selector = Selector.open();

            //在门口翻牌子，正在营业
            serverSocket.register(selector, SelectionKey.OP_ACCEPT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void listener(){
        System.out.println("listen on "+this.port +".");
        //轮询的主线程。每一次轮询只能做一件事：同步。
        try {
            while (true){
                selector.select();
                Set<SelectionKey> keys = selector.selectedKeys();
                //不断迭代就是轮询
                Iterator<SelectionKey> iterator = keys.iterator();
                //同步体现在这里，每次只能拿一个key。只能处理一种状态。每个key代表一种状态。
                while (iterator.hasNext()){
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    process(key);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 具体办理业务的方法。
     * @param key
     * @throws IOException
     */
    private void process(SelectionKey key) throws IOException {
        //针对每一种状态给出一个反应
        if (key.isAcceptable()){//数据准备就绪
            //这个方法体现在非阻塞，不管是数据有没有准备好，都会给我一个状态。
            ServerSocketChannel server = (ServerSocketChannel) key.channel();
            SocketChannel channel = server.accept();
            //要设置为非阻塞，才会往下执行。
            channel.configureBlocking(false);//
            //当数据准备就绪的时候，将状态改为可读
            channel.register(selector,SelectionKey.OP_READ);
        }else if (key.isReadable()){//数据可读
            //key.channel  从多路复用器中拿到客户端的引用。
            SocketChannel channel = (SocketChannel) key.channel();
            int len = channel.read(buffer);
            if (len > 0){
                buffer.flip();
                String content;
                content = new String(buffer.array(), 0, len);
                key = channel.register(selector,SelectionKey.OP_WRITE);
                //
                key.attach(content);
                System.out.println("读取内容:"+content);
            }
        }else if (key.isWritable()){
            SocketChannel channel = (SocketChannel) key.channel();
            String content = (String) key.attachment();
            channel.write(ByteBuffer.wrap(("输出："+content).getBytes()));
            channel.close();
        }
    }

    public static void main(String[] args) {
        new NIOServerDemo(9999).listener();

    }
}
