package com.gupao.vip.netty.rpc.protocol;

import java.io.Serializable;

/**
 * 协议 传输的内容
 * 协议：传输的内容，格式。
 */
public class InvokerProtocol implements Serializable {

    private String className; //类名
    private String methodName; //方法名称
    private Class<?> [] params;//形参列表
    private Object[] values; //实参列表






    public String getClassName() {
        return className;
    }

    public InvokerProtocol setClassName(String className) {
        this.className = className;
        return this;
    }

    public String getMethodName() {
        return methodName;
    }

    public InvokerProtocol setMethodName(String methodName) {
        this.methodName = methodName;
        return this;
    }

    public Class<?>[] getParams() {
        return params;
    }

    public InvokerProtocol setParams(Class<?>[] params) {
        this.params = params;
        return this;
    }

    public Object[] getValues() {
        return values;
    }

    public InvokerProtocol setValues(Object[] values) {
        this.values = values;
        return this;
    }
}
