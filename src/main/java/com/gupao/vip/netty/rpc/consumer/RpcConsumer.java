package com.gupao.vip.netty.rpc.consumer;

import com.gupao.vip.netty.rpc.api.IRpcHelloService;
import com.gupao.vip.netty.rpc.api.IRpcService;
import com.gupao.vip.netty.rpc.consumer.proxy.RpcProxy;
import com.gupao.vip.netty.rpc.provider.IRpcHelloServiceImpl;
import com.gupao.vip.netty.rpc.provider.RpcServiceImpl;

public class RpcConsumer {

    public static void main(String[] args) {
        IRpcHelloService helloService = RpcProxy.create(IRpcHelloService.class);
        System.out.println(helloService.sayHello("tom"));
        IRpcService service = RpcProxy.create(IRpcService.class);
        System.out.println("8 + 2 = "+service.add(8,2));


    }
}
