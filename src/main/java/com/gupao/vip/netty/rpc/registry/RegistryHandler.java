package com.gupao.vip.netty.rpc.registry;

import com.gupao.vip.netty.rpc.protocol.InvokerProtocol;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.omg.CORBA.OBJ_ADAPTER;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RegistryHandler extends ChannelInboundHandlerAdapter{


    private List<String> classNames = new ArrayList<>();
    private Map<String,Object> registryMap = new ConcurrentHashMap<>();






    public RegistryHandler() {
        //1.根据包名，将所有符合条件的class全部扫描出来，放到一个容器中。
        //如果是分布式就去读配置文件(服务名和服务地址)
        scannerClass("com.gupao.vip.netty.rpc.provider");
        //2.给每一个对应的class起一个唯一的名字作作为服务名称，保存到一个容器中。
        doRegistry();
    }

    private void doRegistry() {
        if (classNames.isEmpty())return;
        for (String className : classNames) {
            try {
                Class<?> clazz = Class.forName(className);
                Class<?> i = clazz.getInterfaces()[0];
                //把接口名称作为服务名
                String serviceName = i.getName();
                //本来这里存放的是网络路径，从配置文件读取。
                //在调用的时候再去解析，这里直接用反射调用。
                registryMap.put(serviceName,clazz.newInstance());


            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }

    }
    //正常来说应该是扫描配置文件，这里直接扫描本地class
    private void scannerClass(String packageName) {
        URL url = this.getClass().getClassLoader().getResource(packageName.replaceAll("\\.", "/"));
        File file = new File(url.getFile().substring(1));
        for (File f : file.listFiles()) {
            if (f.isDirectory()){
                scannerClass(packageName+"."+f.getName());
            }else{
                classNames.add(packageName+"."+f.getName().replace(".class",""));
            }

        }


    }

    //客户端发生异常的方法 异常了会回调
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
    //3.当有客户端连接之后，netty会自动把信息解析为一个具体的对象invokerProtocol 即协议内容。
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        InvokerProtocol request = (InvokerProtocol) msg;
        Object result = new Object();
        //4.去注册好的容易中找到符合符合条件的服务
        if (registryMap.containsKey(request.getClassName())){
            Object service = registryMap.get(request.getClassName());
            Method method = service.getClass().getMethod(request.getMethodName(), request.getParams());
            result = method.invoke(service, request.getValues());
        }
        //5.通过远程调用provider得到返回结果，并回复给客户端。
        ctx.write(result);
        ctx.flush();
        ctx.close();
    }

    //客户端建立连接的方法，客户端连接上的时候会发生一个回调



}
