package com.gupao.vip.netty.rpc.provider;

import com.gupao.vip.netty.rpc.api.IRpcHelloService;

public class IRpcHelloServiceImpl implements IRpcHelloService {
    @Override
    public String sayHello(String name) {
        System.out.println(name);
        return "hello:"+name;
    }
}
