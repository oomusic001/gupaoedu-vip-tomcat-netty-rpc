package com.gupao.vip.netty.rpc.api;

public interface IRpcHelloService {
    public String sayHello(String name);
}
