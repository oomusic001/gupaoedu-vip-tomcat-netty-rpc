package com.gupao.vip.netty.rpc.consumer.proxy;

import com.gupao.vip.netty.rpc.protocol.InvokerProtocol;
import com.gupao.vip.netty.rpc.registry.RegistryHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class RpcProxy {

    public static<T> T create(Class<?> clazz){
        MethodProxy proxy = new MethodProxy(clazz);
        T result = (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, proxy);
        return result;
    }
    //将本地调用通过代理的形式变成网络调用。
    private static class MethodProxy implements InvocationHandler {
        private Class<?> clazz;

        public MethodProxy(Class<?> clazz) {
            this.clazz = clazz;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            //如果是实现类
            if (Object.class.equals(method.getDeclaringClass())) {
                return method.invoke(this, args);
            } else {//如果是接口
                return rpcInvoker(proxy, method, args);
            }
        }

        private Object rpcInvoker(Object proxy, Method method, Object[] args) {
            //先构造一个协议内容，消息
            InvokerProtocol msg = new InvokerProtocol();
            msg.setClassName(this.clazz.getName());
            msg.setMethodName(method.getName());
            msg.setParams(method.getParameterTypes());
            msg.setValues(args);
            final RpcProxyHandler proxyHandler = new RpcProxyHandler();
            //发起网络请求
            EventLoopGroup worker = new NioEventLoopGroup();

            Bootstrap client = new Bootstrap();
            client.group(worker).channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY, true)//设置为长连接
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            //在netty中把所有的业务逻辑处理，全部归总到一个队列中。
                            //队列中包含了各种各样的处理逻辑，对这些逻辑在netty中有一个封装
                            //封装成一个对象，一个无锁化串行的任务队列，对象叫做：Pipline(决定了执行任务的顺序)
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            //用户首次连接会将网络数据携带过来，可能是数组，字符串，二进制等。


                            //pipeline就是对处理逻辑的封装。
                            //这里是不知道客户端传来的是什么，需要进行编码解码
                            //int maxFrameLength,
                            // int lengthFieldOffset,
                            // int lengthFieldLength,
                            // int lengthAdjustment,
                            // int initialBytesToStrip

                            //自定义解码器  对自定义的协议内进行编码解码 这两个是还原出invokerProtocol对象
                            pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 4, 4, 0, 4)); //可以使用自定义协议
                            //自定义编码器
                            pipeline.addLast(new LengthFieldPrepender(4));

                            //实参处理  这两个是还原出private Class<?> [] params; private Object[] values;
                            //就是反序列化成java能够识别的语法对象。
                            pipeline.addLast("encoder", new ObjectEncoder());
                            pipeline.addLast("decoder", new ObjectDecoder(
                                    Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
                            //编码解码就是完成对数据的解析
                            //最后一步，执行属于自己的逻辑
                            //1.对传过来的对象进行注册。给每一个对象起一个名字，对外提供服务的名字
                            //2.服务位置要做一个登记
                            pipeline.addLast(proxyHandler);
                        }
                    });
            ChannelFuture future = null;
            try {
                future = client.connect("localhost", 9999).sync();
                future.channel().writeAndFlush(msg).sync();
                future.channel().closeFuture().sync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {

                worker.shutdownGracefully();
            }

            return proxyHandler.getResponse();
        }
    }
}
