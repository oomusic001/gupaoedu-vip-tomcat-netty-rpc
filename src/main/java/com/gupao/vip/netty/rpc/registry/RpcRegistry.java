package com.gupao.vip.netty.rpc.registry;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;



public class RpcRegistry {

    private int port;

    public RpcRegistry(int port) {
        this.port = port;
    }
    //启动方法
    public void start(){
        //servletSocket  ,  serverSocketChannel
        //netty基于nio实现
        //nio很重要的是selector，可以认为是主线程，此外还有worker线程
        //两个线程的关系是怎么维护呢？
        //ServerBootstrap 提供了一个方法进行维护  参数是group
        //nThreads默认是电脑线程的2倍

        //初始化主线程池  nThreads默认是电脑线程的2倍  (带参构造器)
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        //子线程池初始化  具体对应客户端的处理逻辑
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        ServerBootstrap server = new ServerBootstrap();
        server.group(bossGroup,workerGroup)
                //指定selector ，选择应用模型，轮询模型，轮询selectKey
        .channel(NioServerSocketChannel.class)
                //指定子线程谁来执行应用逻辑
        .childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                //在netty中把所有的业务逻辑处理，全部归总到一个队列中。
                //队列中包含了各种各样的处理逻辑，对这些逻辑在netty中有一个封装
                //封装成一个对象，一个无锁化串行的任务队列，对象叫做：Pipline(决定了执行任务的顺序)
                ChannelPipeline pipeline = socketChannel.pipeline();
                //用户首次连接会将网络数据携带过来，可能是数组，字符串，二进制等。


                //pipeline就是对处理逻辑的封装。
                //这里是不知道客户端传来的是什么，需要进行编码解码
                //int maxFrameLength,
                // int lengthFieldOffset,
                // int lengthFieldLength,
                // int lengthAdjustment,
                // int initialBytesToStrip

                //自定义解码器  对自定义的协议内进行编码解码 这两个是还原出invokerProtocol对象
                pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE,4,4,0,4)); //可以使用自定义协议
                //自定义编码器
                pipeline.addLast(new LengthFieldPrepender(4));







                //实参处理  这两个是还原出private Class<?> [] params; private Object[] values;
                //就是反序列化成java能够识别的语法对象。
                pipeline.addLast("encoder",new ObjectEncoder());
                pipeline.addLast("decoder",new ObjectDecoder(
                        Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));




                //编码解码就是完成对数据的解析
                //最后一步，执行属于自己的逻辑
                //1.对传过来的对象进行注册。给每一个对象起一个名字，对外提供服务的名字
                //2.服务位置要做一个登记
                pipeline.addLast(new RegistryHandler());
            }
        }).option(ChannelOption.SO_BACKLOG,128)
        .childOption(ChannelOption.SO_KEEPALIVE,true);
        try {
            //正式启动服务，相当于一个死循环开始轮询。
            ChannelFuture future = server.bind(this.port).sync();
            System.out.println("GP RPC Registry start listen at "+this.port);
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        new RpcRegistry(9999).start();
    }
}
